all: main multvar ag1 ag2

main: main.c
	gcc -Wall main.c -o main -lm -g

ag1: ag1.c ag.h
	gcc -Wall -c ag1.c -o ag1.o -lm -g

ag2: ag2.c ag.h ag1.o
	gcc -Wall ag2.c ag1.o -o ag2 -lm -g

multvar: multvar.c
	gcc -Wall multvar.c -o multvar -lm -g

clean:
	rm -f ag1 ag2 multvar
