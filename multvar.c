#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define POP 100
#define MAX_X 800

double funcao(double);
int ranking(double *, int *);
int reproducao(double *, int *);
void elitismo(double *agente, int *rank);
void roleta(double *agente, int *rank);
void mutacao(double *agente, int *rank, double TaxMut);
void predacao (double* agentes, int* rank, int TaxPred);

//Paramentros:
//
//- Valor inicial da taxa de mutação
//- Multiplicadores da taxa de mutação
//- Numero de indivíduos
//- Gerações
//- Threshold

int main () {
	
	char aux;
	int gen = 0;
	double mut = 1.50;
	printf("Inicialização do Algoritmo Evolutivo\n");
	printf("Numero de gerações: ");
	scanf("%d", &gen);

    typedef struct AGENTE{
        int n = 2;
        int parametros[2];
    }AGENTE;

    // Inicialização dos 'agentes'
    AGENTE agentes[POP];
    melhor = 0.00;

	// Inicialização dos 'agentes'
	double melhor = 0.00;
	srand(time(NULL));
	for(int i = 0; i < POP; i++){
		for(int j = 0; j< agentes[i].n; j++){
			agentes[i].parametros[j] = (double) (rand() % (MAX_X * 100))/100.00;
		}
	}

	int iter = 0, rank[POP];

	while(iter<=gen) {
		iter++;
		
		ranking(agentes, rank);

		if((agentes[rank[0]]-melhor) < 10e-9){
			if(mut*1.5 < 100){
				mut = mut*1.5;
			}
		}else mut = mut/3;
		
		//Atribue ao melhor se for realmente melhor
		if (funcao(melhor) < funcao(agentes[rank[0]])){
			melhor = agentes[rank[0]];
		}

		printf("Melhor: %.9lf\n", melhor);
		//reproducao(agentes, rank);
		roleta(agentes, rank);
		//elitismo(agentes, rank);
		printf("%.9lf\n", funcao(melhor));

		mutacao(agentes, rank, mut);
		if ((gen > 100) & (iter % (int)(0.01*gen) == 0)){
			predacao (agentes, rank, 50);
		}
		if (iter%1000 == 0){
			scanf("%c", &aux);
		}
	}
}

/**
 * Função objetivo, método de avaliação, deve ser maximizada.
 */
double funcao(AGENTE agentes) {
	// if (x > 10)
	// 	return (20.00 - x)*2;
	// else
	// 	return 2*x;
	x = agentes.parametros[0];
	y = agentes.parametros[1];
	
	return -(pow(x-16),2)- pow((y-8),2) + 16	
}


/**
 * Função que retorna as posições dos melhores agentes. Isto é, o
 * primeiro valor corresponde à posição do melhor agente.
 */
int ranking(AGENTE *vetor, int *rank) {
	struct dupla {
		double valor;
		int pos;
	};

	if(vetor == NULL)
		return -1;

	struct dupla auxiliar[POP];
	for(int i = 0; i < POP; i++) {
		auxiliar[i].valor = funcao(vetor[i]);
		auxiliar[i].pos = i;
	}

	// sorting maior pro menor
	struct dupla aux;
	for(int i = 0; i < POP; i++) {
		for(int j = i + 1; j < POP; j++) {
			if (auxiliar[j].valor > auxiliar[i].valor) {
				aux = auxiliar[i];
				auxiliar[i] = auxiliar[j];
				auxiliar[j] = aux;
			}
		}
	}

	for(int i = 0; i < POP; i++) {
		//printf("Posição %d, valor %lf\n", auxiliar[i].pos, auxiliar[i].valor);
		rank[i] = auxiliar[i].pos;
	}
	return 0;
}

/**
 * Função que gera nova geração de agentes.
 */
int reproducao(double *agente, int *rank) {
	double filhos[POP];
	for (int i = 0; i < 5; i++)
		filhos[i] = (agente[rank[0]] + agente[rank[rand() % POP]]) / 2.000;

	for (int i = 0; i < 3; i++)
		filhos[5 + i] = (agente[rank[1]] + agente[rank[rand() % POP]]) / 2.000;

	for (int i = 0; i < 2; i++)
		filhos[8 + i] = (agente[rank[2]] + agente[rank[rand() % POP]]) / 2.000;

	for (int i = 0; i < 5; i++)
		filhos[10 + i] = (agente[rank[3 + i]] + agente[rank[rand() % POP]]) / 2.000;

	for (int i = 0; i < POP; i++)
		agente[i] = filhos[i];
	return 0;
}

void elitismo(double *agente, int *rank){ //media do melhor com todos
	double filhos[POP];
	for (int i = 0; i < POP; i++)
		filhos[i] = (agente[rank[0]] + agente[i]) / 2.000;

	for (int i = 0; i < POP; i++)
		agente[i] = filhos[i];
}

void roleta(AGENTE* agente, int *rank){ //esquema de roleta com os 3 mlehores
	double pri = 40.00, seg = 35.00, ter = 25.00;
	double r;
	AGENTE filhos[POP];

	for (int i = 0; i < POP; i++){
		r = (rand() % 10000)/100;//gera ujm double entre 0 e 100

		if(i != rank[0]){ //protege o melhor
			if(r > 0 && r <= pri){//o melhor
				for(int j = 0, j< agente.n; j++){
					filhos[i].parametros[j] = (agente[rank[0].parametros[j]] + agente[i].parametros[j]) / 2.000;
				}

			}else if(r > pri && r <= (seg+pri)){//o segundo
				for(int j = 0, j< agente.n; j++){
					filhos[i].parametros[j] = (agente[rank[1].parametros[j]] + agente[i].parametros[j]) / 2.000;
				}

			}else if(r > (seg+pri) && r <= (ter+seg+pri)){//o terceiro
				for(int j = 0, j< agente.n; j++){
					filhos[i].parametros[j] = (agente[rank[2].parametros[j]] + agente[i].parametros[j]) / 2.000;
				}

			}
			else{
				for(int j = 0, j< agente.n; j++){
					filhos[i].parametros[j] = (agente[rank[0].parametros[j]] + agente[i].parametros[j]) / 2.000;
				}
			}
		}
	}

	for (int i = 0; i < POP; i++)
		agente[i] = filhos[i];
}


void mutacao(double *agente, int *rank, double TaxMut){

	for(int i=0; i<POP; i++){
		if(i != rank[0])
			agente[i] = agente[i] + (double) (((rand()%MAX_X - (MAX_X/2.0))/100.0) * TaxMut);
		if(agente[i] > MAX_X || agente[i] < 0)
			agente[i] = (double) (rand() % (MAX_X * 100))/100.00;
	}
}

void predacao (double* agentes, int* rank, int TaxPred){
	int array [POP];
	int temp;

	for(int i = 0; i<POP; i++){
		array[i] = 0;
	}

	for(int i = 0; i<TaxPred; i++){
		temp = rand()%POP;
		if (array[temp] == 0){
			agentes[temp] = (double) (rand() % (MAX_X * 100))/100.00;
			array[temp] = 1;
		}
	}
}
