#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "ag.h"

#define POP 100

int main () {
	int gen = 0;
	//double mut = 1.50;
	printf("Inicialização do Algoritmo Evolutivo\n");
	printf("Numero de gerações: ");
	scanf("%d", &gen);

	melhor best;
	best.resultado = -1;

	// Inicialização dos 'agentes'
	gene *agentes[POP];
	double resultados[POP];
	srand(time(NULL));
	for(int i = 0; i < POP; i++){
		agentes[i] = gera_gene2();
	}

	int iter = 0, rank[POP];

	while(iter<=gen) {
		
		iter++;
		for(int i = 0; i < POP; i++) {
			resultados[i] = funcao2(agentes[i],ag1(agentes[i]));
		}
		ranking2(resultados, rank);
		
		/*
		if((agentes[rank[0]]-best.resultado) < 10e-9){
			if(mut*1.5 < 100){
				mut = mut*1.5;
			}
		}else mut = mut/3;
		*/

		//Atribue ao melhor se for realmente melhor
		if (best.resultado < resultados[rank[0]]){
			best.gene_melhor = agentes[rank[0]];
			best.resultado = resultados[rank[0]];
		}

		//reproducao(agentes, rank);
		roleta2(agentes, rank);
		//elitismo(agentes, rank);
		printf("%.9lf\n", best.resultado);

		//mutacao2(agentes, rank, mut);
		if ((gen > 100)){ 
			if ((iter % (int)(0.01*gen)) == 0){
				predacao2(agentes, rank, 50);
					
			}
		}
	}
}



/**
 * Gera um gene aleatorio
 */
gene* gera_gene2(){

	gene* individuo = (gene *) malloc(sizeof(gene));
	individuo->mutacao = rand()%6; 
	individuo->metodo_selecao = rand()%3; 
	individuo->pop_inicial = 100;
	individuo->qtd_predacao = rand()%(individuo->pop_inicial/2+1); 
	individuo->geracoes = 2000;
	individuo->threshold = 10e-7;

	return individuo;
}

/**
 * Gera um filho de dois agentes
 */
gene * gera_filho2(gene *pai, gene *mae) {
	if (pai == NULL || mae == NULL) {
		printf("Erro: agente pai ou mae nulo na reprodução\n");
		return NULL;
	}
	gene* filho = (gene *) malloc(sizeof(gene));
	filho->mutacao = (pai->mutacao + mae->mutacao) / 2.0; 
	filho->metodo_selecao = (int) (pai->metodo_selecao + mae->metodo_selecao) / 2;
	filho->qtd_predacao =(pai->qtd_predacao + mae->qtd_predacao) / 2.0;
	filho->pop_inicial = 100;
	filho->geracoes = 1000;
	filho->threshold = 0.00001;

	return filho;
}


/**
 * Função objetivo, método de avaliação, deve ser maximizada.
 */
double funcao2(gene * x, result_simulacao * y) {
	// queremos atingir o resultado no menor número de iterações possível
	return (x->geracoes - y->n_geracoes);
}


/**
 * Função que retorna as posições dos melhores agentes. Isto é, o
 * primeiro valor corresponde à posição do melhor agente.
 */
int ranking2(double *resultados, int *rank) {
	struct dupla {
		double valor;
		int pos;
	};

	if(resultados == NULL)
		return -1;

	struct dupla auxiliar[POP];
	for(int i = 0; i < POP; i++) {
		auxiliar[i].valor = resultados[i];
		auxiliar[i].pos = i;
	}

	// sorting
	struct dupla aux;
	for(int i = 0; i < POP; i++) {
		for(int j = i + 1; j < POP; j++) {
			if (auxiliar[j].valor > auxiliar[i].valor) {
				aux = auxiliar[i];
				auxiliar[i] = auxiliar[j];
				auxiliar[j] = aux;
			}
		}
	}

	for(int i = 0; i < POP; i++) {
		//printf("Posição %d, valor %lf\n", auxiliar[i].pos, auxiliar[i].valor);
		rank[i] = auxiliar[i].pos;
	}
	return 0;
}

/**
 * Função que gera nova geração de agentes.
 */
void reproducao2(gene** agente, int *rank) {
	gene *filhos[POP];
	for (int i = 0; i < 5; i++)
		filhos[i] = gera_filho2(agente[rank[0]], agente[rank[rand() % POP]]);

	for (int i = 0; i < 3; i++)
		filhos[5 + i] = gera_filho2(agente[rank[1]], agente[rank[rand() % POP]]);

	for (int i = 0; i < 2; i++)
		filhos[8 + i] = gera_filho2(agente[rank[2]], agente[rank[rand() % POP]]);

	for (int i = 0; i < 5; i++)
		filhos[10 + i] = gera_filho2(agente[rank[3 + i]], agente[rank[rand() % POP]]);

	for (int i = 0; i < POP; i++)
		agente[i] = filhos[i];
}

void elitismo2(gene** agente, int *rank){ //media do melhor com todos
	gene *filhos[POP];
	for (int i = 0; i < POP; i++)
		filhos[i] = gera_filho2(agente[rank[0]], agente[i]);

	for (int i = 0; i < POP; i++)
		agente[i] = filhos[i];
}

void roleta2(gene** agente, int *rank){ //esquema de roleta com os 3 mlehores
	double pri = 40.00, seg = 35.00, ter = 25.00;
	double r;
	gene *filhos[POP];

	for (int i = 0; i < POP; i++){
		r = (rand() % 10000)/100;//gera ujm double entre 0 e 100

		if(i != rank[0]){ //protege o melhor
			if(r > 0 && r <= pri){//o melhor
				filhos[i] = gera_filho2(agente[rank[0]], agente[i]);

			}else if(r > pri && r <= (seg+pri)){//o segundo
				filhos[i] = gera_filho2(agente[rank[1]], agente[i]);

			}else if(r > (seg+pri) && r <= (ter+seg+pri)){//o terceiro
				filhos[i] = gera_filho2(agente[rank[2]], agente[i]);

			}else{//caso as probabilidades nao somem 100 da preferencia para o melhor
				filhos[i] = gera_filho2(agente[rank[0]], agente[i]);
			}
		}else{
			filhos[i] = agente[i];
 		}
	}

	for (int i = 0; i < POP; i++)
		agente[i] = filhos[i];
}

/*
void mutacao2(gene *agente, int *rank, double TaxMut){

	for(int i=0; i<POP; i++){
		if(i != rank[0])
			agente[i]->mutacao = agente[i]->mutacao + (double) (((rand()%MAX_X - (MAX_X/2.0))/100.0) * TaxMut);
			//agente[i]->metodo_selecao = (agente[i]->metodo_selecao + (((rand()%MAX_X - (MAX_X/2.0))/100.0) * TaxMut)) % 3;
			agente[i]->qtd_predacao = agente[i]->qtd_predacao + (double) (((rand()%MAX_X - (MAX_X/2.0))/100.0) * TaxMut);
		if(agente[i] > MAX_X || agente[i] < 0)
			agente[i] = (double) (rand() % (MAX_X * 100))/100.00;
	}
}
*/

void predacao2 (gene** agentes, int* rank, int TaxPred){
	int array [POP];
	int temp;

	for(int i = 0; i<POP; i++){
		array[i] = 0;
	}

	for(int i = 0; i<TaxPred; i++){
		temp = rand()%POP;
		if (array[temp] == 0){
			agentes[temp] = gera_gene2();
			array[temp] = 1;
		}
	}
}
